# smartChickenHome

## Einleitung
Ein Smart-Home hat heute jeder, warum sollen es Hühner schlechter haben?
In diesem Projekt ist der technische Teil des Smart-Chicken-Homes dokumentiert.
Zum Nachbau und als Anregung.
Viel Spaß!

## Stückliste
<table border="1">
 <tr>
  <td> Was </td>
  <td> Anzahl </td>
  <td> Beschreibung </td>
 </tr>
 <tr>
  <td> Raspberry Pi </td>
  <td> 1x </td>
  <td> Raspberry Pi 3 mit WLAN </td>
 </tr>
 <tr>
  <td> Raspberry Pi GPIO Expansion Board</td>
  <td> 1x </td>
  <td> COM-FOUR RaspberryPi Prototyping Board mit Schraubklemmen zum Aufstecken auf den Raspberry PI </td>
 </tr>
 <tr>
  <td> USB-Stecker  </td>
  <td> 1x </td>
  <td> Adapter, 5 Pin Terminalblock - Micro USB 2.0 Typ B Stecker Schraubmontage </td>
 </tr>
 <tr>
  <td> Türservo </td>
  <td> 1x </td>
  <td> Segelwindenservo "Multiplex HS-785HB (S)" </td>
 </tr>
 <tr>
  <td> Kamera </td>
  <td> 1x </td>
  <td> ELP-Camera 2MP w/ 170&deg; fisheye USB (IR-Filter entfernen)</td>
 </tr>
 <tr>
  <td> Temperatur/Feuchte </td>
  <td> 1x </td>
  <td> AM2301 (DHT21) </td>
 </tr>
 <tr>
  <td> Relais (4-Kanal)</td>
  <td> 1x </td>
  <td> 4 Kanal Relais 5V/230V Raspberry Pi Optokoppler Modul Channel Relay Arduino </td>
 </tr>
 <tr>
  <td> Netzteil </td>
  <td> 1x </td>
  <td> Netzteil 12V+5V 2A für IDE Festplatten CD DVD Brenner m. Molexstecker Stromkabel</td>
 </tr>
 <tr>
  <td> Beleuchtung </td>
  <td> n x </td>
  <td> LED-Stripe 12V 2865 o.ä. mit Anschluß-Clips </td>
 </tr>
 <tr>
  <td> IR Scheinwerfer </td>
  <td> 1 x </td>
  <td> Kingbright BL0106-15-28 IR-Emitter 940 nm 40 ° 5 mm + Vorwiderstand</td>
 </tr>
 <tr>
  <td> Gehäuse </td>
  <td> 1 x </td>
  <td> 8TE Installationskasten (außen) </td>
 </tr>
 <tr>
  <td> Steuerkabel </td>
  <td> n x </td>
  <td> LAPP 0028404 Datenleitung UNITRONIC® LiYY 4 x 0.34 mm² Kiesel-Grau (RAL 7032) </td>
 </tr>
 <tr>
  <td> Kleinteile </td>
  <td> n x </td>
  <td> Kabelkanal, Pfostenstecker und -buchsen, Schrumpfschlauch, Isolierband, selbstverschweißendes Klebeband, Heißkleber </td>
 </tr>
</table>