#!/usr/bin/python

import RPi.GPIO as GPIO

# Pinout
lichtPIN=23
irPIN=24
servopowerPIN=27
relais4PIN=22
servosignalPIN=17
dht11signalPIN=18

#
# Pinout nach SoC-Konvention
GPIO.setmode(GPIO.BCM)

#
# Pinmodes setzen
GPIO.setup(lichtPIN, GPIO.OUT)
GPIO.setup(irPIN, GPIO.OUT)
GPIO.setup(servopowerPIN, GPIO.OUT)
GPIO.setup(relais4PIN, GPIO.OUT)
GPIO.setup(servosignalPIN, GPIO.OUT)

