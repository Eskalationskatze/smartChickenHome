#!/usr/bin/python

import RPi.GPIO as GPIO
import time
GPIO.setwarnings(False)
execfile('./initgpio.py') 

GPIO.setmode(GPIO.BCM)
# Stromversorgung für Servo einschalten
# Achtung: Modellbau-Servos werden immer auf der Masseleitung
# geschaltet, nicht auf VCC!
GPIO.output(servopowerPIN, GPIO.HIGH)

p = GPIO.PWM(servosignalPIN, 50) # GPIO 17 als PWM mit 50Hz
# Die untere Position "Tür zu" ist in meinen Aufbau bei 8% PWM.
# Die obere Position "Tür auf" ist in meinen Aufbau bei 10.75% PWM.
# Der Verfahrweg ist bei meiner Klappe etwa 20cm.
# Die Verfahrzeit sind etwa 5 Sekunden, so dass ich die PWM 10s laufen 
# lasse, bevor ich sie und den Servo wieder abschalte
p.start(8.0) # Initialisierung mit 8% PWM
time.sleep(10)
p.stop()

# Stromversorgung für Servo wieder ausschalten
GPIO.output(servopowerPIN, GPIO.LOW)


